#!/bin/bash
#Script atualiza variaveis no Gitlab.
#Executa script env.sh para setar variaveis
echo
echo "Iniciando Script"
DIR="$(pwd)"
eval $(<env.sh)

#declare que o script deve esperar a execucao terminar
wait

#Ajustar Url do Projeto no Gitlab.

VARPROJECT="https://gitlab.com/api/v4/projects/3770930/variables"
DOCKER_CA="$(cat $DIR/ca.pem)"
DOCKER_CERT="$(cat $DIR/cert.pem)"
DOCKER_KEY="$(cat $DIR/key.pem)"
UPDATE="curl --request PUT --header"
#Ajustar Token.

### Documentação do Token (https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
TOKEN="PRIVATE-TOKEN: hPoLRXgZeR4zhyBvkW7w"
echo 
echo "Gravando Docker_CA"
$UPDATE "$TOKEN" "$VARPROJECT/DOCKER_CA" --form "value=$DOCKER_CA" 1&> /dev/null 2&> /dev/null
echo

echo "Gravando Docker_CE"
$UPDATE "$TOKEN" "$VARPROJECT/DOCKER_CERT" --form "value=$DOCKER_CERT" 1&> /dev/null 2&> /dev/null
echo

echo "Gravando Docker_Key"
$UPDATE "$TOKEN" "$VARPROJECT/DOCKER_KEY" --form "value=$DOCKER_KEY" 1&> /dev/null 2&> /dev/null
echo

echo "Gravar o nome do Serviço"
read -p "Digite o Nome do Serviço do Docker - depois aperte <Enter> para finalizar : "  RESP
$UPDATE "$TOKEN" "$VARPROJECT/DOCKER_SERVICE" --form "value=$RESP" 1&> /dev/null 2&> /dev/null
echo

echo "Imprimindo Url do Play-With-Docker"
echo $DOCKER_HOST
$UPDATE "$TOKEN" "$VARPROJECT/DOCKER_URL" --form "value=$DOCKER_HOST" 1&> /dev/null 2&> /dev/null

clear
exit
